package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testCelciusRegular() {
		int celsius = Celsius.fromFahrenheit(77);
		assertTrue("Invalid temperature", celsius == 25);
	}
	
	@Test
	public void testCelciusException() {
		int celsius = Celsius.fromFahrenheit(75);
		assertFalse("Invalid temperature", celsius == 23.8889);
	}
	
	@Test
	public void testCelciusBoundaryIn() {
		int celsius = Celsius.fromFahrenheit(75);
		assertTrue("Invalid temperature", celsius == 24);
	}
	
	@Test
	public void testCelciusBoundaryOut() {
		int celsius = Celsius.fromFahrenheit(75);
		assertFalse("Invalid temperature", celsius == 23);
	}

}
