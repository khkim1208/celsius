package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int arg) {
		
		double c = (double)(arg-32) * 5/9;
		int result = (int)Math.round(c);
		
		return result;
	}
	
	

}